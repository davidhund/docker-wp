@ECHO OFF

:: local batch script to run Docker Wordpress services
:: ========================================================

IF "%1"=="" GOTO USAGE
IF "%1"=="help" GOTO USAGE
IF "%1"=="up" GOTO UP
IF "%1"=="ssh" GOTO SSH
IF "%1"=="stop" GOTO STOP
IF "%1"=="down" GOTO DOWN
IF "%1"=="backup" GOTO DBBACKUP
IF "%1"=="log" GOTO LOG
IF "%1"=="id" GOTO ID
IF "%1"=="ip" GOTO IP
IF "%1"=="link" GOTO LINK
GOTO :EOF

:: NOTE: issues with FTP credentials for changing plugins??
:: cd /var/www/html
:: sudo chown -R www-data:www-data src/wp-content

:: `local up` - run services
:: --------------------------------------------------------
:UP
CLS
ECHO   ------------------------------------------------------
ECHO   -- Starting Docker Wordpress services...            --
ECHO   ------------------------------------------------------
docker-compose up -d && start "" http://localhost:8000
ECHO ------------------------------------------------------
ECHO:
ECHO     If your browser shows ERR_EMPTY_RESPONSE,
ECHO     wait a bit and reload...
GOTO :EOF


:: `local stop` - Stop services
:: --------------------------------------------------------
:STOP
CLS
ECHO   ------------------------------------------------------
ECHO   -- Stopping Docker Wordpress services...            --
ECHO   ------------------------------------------------------
docker-compose stop
ECHO   ------------------------------------------------------
GOTO :EOF


:: `local down` - Destroy containers
:: --------------------------------------------------------
:DOWN
CLS
ECHO   ------------------------------------------------------
ECHO   -- Destroying Docker Wordpress services...          --
ECHO   ------------------------------------------------------
ECHO:
ECHO      Warning! The complete docker image and all containers
ECHO      will be DESTROYED!
ECHO:
ECHO      Press CTRL - ﻿C to cancel...
ECHO:
PAUSE
docker-compose down
ECHO   ------------------------------------------------------
GOTO :EOF


:: `local ssh` - Enter Bash on Wordpress service
:: --------------------------------------------------------
:SSH
CLS
ECHO   ------------------------------------------------------
ECHO   -- Entering Docker Wordpress services Shell...      --
ECHO   ------------------------------------------------------
ECHO      Type 'exit' to, erm, exit the shell...
ECHO:
docker-compose exec wordpress bash
GOTO :EOF


:: `local link` - Create predefined symlinks
:: --------------------------------------------------------
:LINK
CLS
ECHO   ------------------------------------------------------
ECHO   -- Creating theme/plugin Symlinks in wp-content     --
ECHO   ------------------------------------------------------
ECHO:
:: WIP
:: Example Windows Symlink...
:: mklink /J c:\projects\docker-wp\src\wp-content\themes\<THEME> c:\projects\<THEME>
ECHO   WIP: see local.bat for Windows example...
:: IF EXIST PLUGINS_THEMES.txt (
:: 	FOR /f "tokens=*" %%a in (PLUGINS_THEMES.txt) do (
:: 		mklink /J c:\projects\pm-docker-wp\src\wp-content%%a
:: 	)
:: ) ELSE (
:: 	ECHO NO PLUGINS_THEMES.txt found to create symlinks
:: )
GOTO :EOF


:: `local backup` - Dump MySQL
:: --------------------------------------------------------
:DBBACKUP
CLS
ECHO   ------------------------------------------------------
ECHO   -- Dumping Docker Wordpress MySQL in db-dumps...    --
ECHO   ------------------------------------------------------
ECHO      NOTE: **REMOVES** the 1st [Warning] line
ECHO      in the db_^<date^>.sql file!
ECHO   ------------------------------------------------------
ECHO:
For /f "tokens=2-4 delims=- " %%a in ('date /t') do (set mydate=%%a%%b%%c)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)
docker-compose exec db mysqldump --add-drop-table -u wordpress -pwordpress wordpress > db-dumps/db_%mydate%_%mytime%.sql
ECHO:
sed -e '1d' db-dumps/db_%mydate%_%mytime%.sql > db-dumps/db_latest.sql
GOTO :EOF


:: `local ip` - Return IP
:: --------------------------------------------------------
:IP
CLS
ECHO   ------------------------------------------------------
ECHO   -- Getting IP of Docker Wordpress service...        --
ECHO   ------------------------------------------------------
ECHO      Make sure Docker services are running (*local up*)
ECHO      and optionally add service to get ID of:
ECHO:
ECHO      wp:        local ip wp
ECHO      db:        local ip db
ECHO      adminer:   local ip adminer
ECHO:
ECHO   ------------------------------------------------------
IF "%2"=="" GOTO IPWP
IF "%2"=="db" GOTO IPDB
IF "%2"=="wp" GOTO IPWP
IF "%2"=="adminer" GOTO IPAD
ECHO   ------------------------------------------------------
GOTO :EOF

:IPDB
ECHO Getting IP address for MySQL service: 
ECHO:
docker inspect docker-wp_db_1 | grep IPAddress
GOTO :EOF

:IPWP
ECHO Getting IP address for Wordpress service: 
ECHO:
docker inspect docker-wp_wordpress_1 | grep IPAddress
GOTO :EOF

:IPAP
ECHO Getting IP address for Adminer service: 
ECHO:
docker inspect docker-wp_adminer_1 | grep IPAddress
GOTO :EOF

:: `local log <service>` - Return LOG
:: --------------------------------------------------------
:LOG
CLS
ECHO   ------------------------------------------------------
ECHO   -- Getting LOG of Docker Wordpress service...       --
ECHO   ------------------------------------------------------
ECHO      Make sure Docker services are running (*local up*)
ECHO      and optionally add service to get ID of:
ECHO:
ECHO      Wordpress: local log wp
ECHO      MySQL:     local log db
ECHO      Adminer:   local log adminer
ECHO:
ECHO   ------------------------------------------------------
IF "%2"=="" GOTO LOGWP
IF "%2"=="db" GOTO LOGDB
IF "%2"=="wp" GOTO LOGWP
IF "%2"=="adminer" GOTO LOGAD
GOTO :EOF

:LOGDB
ECHO Getting last log for MySQL: 
PAUSE
docker-compose logs db
GOTO :EOF

:LOGWP
ECHO Getting last log for Wordpress: 
docker-compose logs wordpress
GOTO :EOF

:LOGAD
ECHO Getting last log for Adminer: 
docker-compose logs adminer
GOTO :EOF


:: `local id` - Return ID
:: --------------------------------------------------------
:ID
CLS
ECHO   ------------------------------------------------------
ECHO   -- Getting ID of Docker service...                  --
ECHO   ------------------------------------------------------
ECHO      Make sure Docker services are running (*local up*)
ECHO      and optionally add service to get ID of:
ECHO:
ECHO      Wordpress: local id wp
ECHO      MySQL:     local id db
ECHO      Adminer:   local id adminer
ECHO:
ECHO   ------------------------------------------------------
IF "%2"=="" GOTO IDWP
IF "%2"=="db" GOTO IDDB
IF "%2"=="wordpress" GOTO IDWP
IF "%2"=="adminer" GOTO IDAD
GOTO :EOF

:IDDB
ECHO    MySQL service ID:
ECHO:
docker ps --filter name=docker-wp_db_1 --filter status=running -q
GOTO :EOF

:IDWP
ECHO    Wordpress service ID:
ECHO:
docker ps --filter name=docker-wp_wordpress_1 --filter status=running -q
GOTO :EOF

:IDAD
ECHO    Adminer service ID:
ECHO:
docker ps --filter name=docker-wp_adminer_1 --filter status=running -q
GOTO :EOF


:: `local` - Usage
:: --------------------------------------------------------
:USAGE
CLS
ECHO:
ECHO   ------------------------------------------------------
ECHO   -- Docker Wordpress Usage:                          --
ECHO   ------------------------------------------------------
ECHO      Services:
ECHO:
ECHO      Wordpress: http://localhost:8000
ECHO      Adminer:   http://localhost:8080
ECHO      MySQL:     http://localhost:3306
ECHO:
ECHO      Persisent Volumes:
ECHO:
ECHO      ./src, ./db-dumps, db_data
ECHO   ------------------------------------------------------
ECHO:
ECHO      Usage: %0  ^<CMD^>
ECHO      Where ^<CMD^> is any of:
ECHO:
ECHO      up                   *Starts* WP services
ECHO      stop                 *Stops* the Docker services
ECHO      down                 *Destroys* all Docker services
ECHO      backup               Dumps MySQL in db-dumps/db_^<date^>.sql
ECHO      ssh                  Enter a Shell in the Wordpress service
ECHO      log [db^|wordpress]   Returns latest *Logs* of service
ECHO      ip [db^|wordpress]    Returns *IP* of service
ECHO      id [wp^|db^|adminer]   Returns *ID* of service
ECHO:

:End