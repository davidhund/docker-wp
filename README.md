# Run Wordpress / MySQL / Adminer on Docker for Windows

## Requirements
- Windows 10 with Hyper-V **enabled**
- Docker will ask for access to `C:\`... Allow it.

## Installation
- Download and install [Docker for Windows](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe) (Windows re-login required)
- **Open an Administrator CMD.exe window**
- Check that docker is installed properly: 
- - `docker version`
- - `docker-compose version`
- `cd c:\projects`
- `git clone git@gitlab.com:davidhund/docker-wp.git`
- `cd docker-wp`
- Run `local up`
- - .. A local `./src` folder will be filled with the WP files
- - .. [http://localhost:8000](http://localhost:8000/) should open with a WP site!
- - (If the site does not run, it's because ./src is not done syncing. Wait a bit before F5ing. This is only an issue 1st time)
- - You can access [Adminer (DB Admin) at :8080](http://localhost:8080) and import an existing WP database. Or use the `initial.sql`

### Credentials
- If the WP db is already pre-filled you can access the Admin with credentials `admin/admin`
- Credentials for the docker services can be found in `docker-compose.yml`

## `local` commands

- `local` ( or `local help`) should give some Usage info

### Symlinks

It might be handy to **symlink** existing themes/plugins to c:\projects\docker-wp\src\wp-content\themes|plugins

```bash
# in c:\projects\docker-wp in an Administrator CMD.exe
# All on 1 line & check 'projects' / 'Projects' etc.
mklink /J c:\projects\docker-wp\src\wp-content\themes\example-theme C:\projects\example-theme
```

### Troubleshooting

Sometimes you might run into trouble. I've noticed the following.

#### 👺 Shared volumes (`src/`, `db-dumps/`, ..) do not show up in Finder

This happened to me and I could not figure out why. Fixing the **Shared Drive Credentials** fixed it.

**Solution**
- Shut down: `local down`
- Docker (right-click Docker icon in Dock) > Settings > Shared Drives
- Make sure `C` is selected
- Click **Reset Credentials** > **Apply**
- Re-enable/select `C`
- CLick **Apply** again
- Run `local up`
- .. shared volumes should show up in finder now.


#### 👺 `Error: [...] no matching manifest for windows/amd64 in the manifest list entries.`

This had to do with my Windows Docker app running in 'Windows Container' mode.

**Solution**
- Right click the Docker icon in your dock and click 'Switch to Linux containers...'
- You might need to restart cmd.exe and run `local up` again (or `local down` first)
- Also check if you've *enabled*  `Experimental Features` in Docker > Settings > Deamon... 

#### 👺 PHP.ini file upload error

I've added a custom `uploads.ini` with increased file/memory size.
This should allow for bigger uploads etc. (make sure to rebuild/restart docker)

When that does not work you could increase the file upload size by **adding it to your `.htaccess`**:
https://github.com/docker-library/wordpress/issues/10#issuecomment-72141272