RUN:
- docker-compose up -d
- docker inspect docker-wp_db_1 | grep IPAddress
- docker-compose down removes the containers and default network, but preserves your Wordpress database.
- docker-compose down --volumes removes the containers, default network, and the Wordpress database.
- Command: docker-compose exec wordpress bash #full shell!
- Command: docker-compose exec wordpress /bin/ls -la
- Inspect volumes: docker volume ls & docker volume inspect docker-wp_wp-content

- EXPORT DB: 
docker-compose exec db mysqldump --add-drop-table -u wordpress -pwordpress wordpress > db-dumps/db_`date +"%m%d%y-%H%M%S"`.sql

docker-compose exec db sh -c 'exec mysqldump --add-drop-table -uroot -p"$MYSQL_ROOT_PASSWORD" wordpress' > db-dumps/db_`date +"%m%d%y-%H%M%S"`.sql


Get ID of running container:
https://github.com/mozilla/standup/commit/cc8f57c551d3c765455dd88137db6cacee5f9526
# DB
docker ps --filter name=docker-wp_db_1 --filter status=running -q
# WP
docker ps --filter name=docker-wp_wordpress_1 --filter status=running -q
# Adminer
docker ps --filter name=docker-wp_adminer_1 --filter status=running -q

- IMPORT DB: 
docker-compose exec -T db mysql -u wordpress -pwordpress wordpress < db-dumps/db.sql

- Adminer: localhost:8080

- Logs: docker-compose logs db (of wordpress)

WEB:
- http://localhost:8000 (admin/admin)

SYMLINK:
- wp-content/themes -> C:\projects\local-themes
- wp-content/plugins -> C:\projects\local-plugins